package ru.t1.stepanishchev.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.stepanishchev.tm.dto.request.TaskCreateRequest;
import ru.t1.stepanishchev.tm.event.ConsoleEvent;
import ru.t1.stepanishchev.tm.util.TerminalUtil;

@Component
public final class TaskCreateListener extends AbstractTaskListener {

    @NotNull
    private final static String NAME = "task-create";

    @NotNull
    private final static String DESCRIPTION = "Create new task.";

    @Override
    @EventListener(condition = "@taskCreateListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(getToken(), name, description);
        taskEndpointClient.createTask(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}