package ru.t1.stepanishchev.tm.listener.data;

import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.stepanishchev.tm.api.endpoint.IDomainEndpoint;
import ru.t1.stepanishchev.tm.listener.AbstractListener;

@Component
@NoArgsConstructor
public abstract class AbstractDataListener extends AbstractListener {

    @Autowired
    public IDomainEndpoint domainEndpoint;

}