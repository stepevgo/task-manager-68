package ru.t1.stepanishchev.tm.service.model;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.stepanishchev.tm.api.service.model.ITaskService;
import ru.t1.stepanishchev.tm.enumerated.Status;
import ru.t1.stepanishchev.tm.enumerated.TaskSort;
import ru.t1.stepanishchev.tm.exception.entity.StatusEmptyException;
import ru.t1.stepanishchev.tm.exception.entity.TaskNotFoundException;
import ru.t1.stepanishchev.tm.exception.field.*;
import ru.t1.stepanishchev.tm.model.Task;
import ru.t1.stepanishchev.tm.model.User;
import ru.t1.stepanishchev.tm.repository.model.TaskRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.List;

@Service
@NoArgsConstructor
public final class TaskService extends AbstractUserOwnedService<Task>
        implements ITaskService {

    @NotNull
    @Autowired
    public TaskRepository repository;

    @NotNull
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @NotNull
    public String getSortType(@Nullable final TaskSort sort) {
        if (sort == TaskSort.BY_CREATED) return "created";
        if (sort == TaskSort.BY_STATUS) return "status";
        else return "name";
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Task task = new Task();
        task.setUser(entityManager.find(User.class, userId));
        task.setName(name);
        task.setDescription(description);
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task updateById(
            @Nullable String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();

        @Nullable Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();

        task.setName(name);
        task.setDescription(description);
        repository.save(task);
        return task;
    }

    @Override
    @NotNull
    @SneakyThrows
    @Transactional
    public Task removeById(
            @Nullable String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @Nullable Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        repository.delete(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();

        @Nullable Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();

        task.setStatus(status);
        repository.save(task);
        return task;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteByUserId(userId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        repository.deleteAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(@Nullable String userId, @Nullable TaskSort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAllByUserIdAndSort(userId, getSortType(sort));
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findByUserIdAndId(userId, id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void set(@NotNull final Collection<Task> tasks) {
        if (tasks.isEmpty()) return;
        repository.saveAll(tasks);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        return repository.findAllByUserIdAndProjectId(userId, projectId);
    }

}