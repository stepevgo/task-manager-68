package ru.t1.stepanishchev.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.stepanishchev.tm.api.service.dto.IAbstractDTOService;
import ru.t1.stepanishchev.tm.dto.model.AbstractModelDTO;
import ru.t1.stepanishchev.tm.repository.dto.AbstractDTORepository;

import java.util.Collection;

@Service
@NoArgsConstructor
public abstract class AbstractDTOService<M extends AbstractModelDTO>
        implements IAbstractDTOService<M> {

    @NotNull
    protected AbstractDTORepository<M> repository;

    @Transactional
    public void add(@NotNull final M model) {
        repository.save(model);
    }

    @Transactional
    public void addAll(@NotNull final Collection<M> models) {
        repository.saveAll(models);
    }

    @Transactional
    public void update(@NotNull final M model) {
        repository.save(model);
    }

    @Transactional
    public void remove() {
        repository.deleteAll();
    }

}