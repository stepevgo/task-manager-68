package ru.t1.stepanischev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.stepanischev.tm.api.endpoint.ITaskRestEndpoint;
import ru.t1.stepanischev.tm.model.TaskDTO;
import ru.t1.stepanischev.tm.service.TaskService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@RestController
@RequestMapping("/api/tasks")
//@WebService(endpointInterface = "ru.t1.stepanischev.tm.api.endpoint.ITaskRestEndpoint")
public class TaskRestEndpointImpl implements ITaskRestEndpoint {

    @NotNull
    @Autowired
    private TaskService taskService;

    @Nullable
    @Override
    @GetMapping("/findAll")
//    @WebMethod
    public Collection<TaskDTO> findAll() {
        return taskService.findAll();
    }

    @Nullable
    @Override
    @GetMapping("/findById/{id}")
//    @WebMethod
    public TaskDTO findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        return taskService.findOneById(id);
    }

    @Override
    @PostMapping("/delete")
//    @WebMethod
    public void delete(
            @WebParam(name = "task", partName = "task")
            @RequestBody TaskDTO task
    ) {
        taskService.remove(task);
    }

    @Override
    @PostMapping("/deleteById/{id}")
//    @WebMethod
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        taskService.removeById(id);
    }

    @NotNull
    @Override
    @PostMapping("/save")
//    @WebMethod
    public TaskDTO save(
            @WebParam(name = "task", partName = "task")
            @RequestBody TaskDTO task
    ) {
        taskService.add(task);
        return task;
    }

}