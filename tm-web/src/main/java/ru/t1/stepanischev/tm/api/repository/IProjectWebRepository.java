package ru.t1.stepanischev.tm.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.stepanischev.tm.model.ProjectDTO;

@Repository
public interface IProjectWebRepository extends JpaRepository<ProjectDTO, String> {
}