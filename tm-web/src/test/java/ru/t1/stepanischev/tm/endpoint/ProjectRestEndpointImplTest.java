package ru.t1.stepanischev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.stepanischev.tm.client.ProjectRestEndpointClient;
import ru.t1.stepanischev.tm.marker.IntegrationCategory;
import ru.t1.stepanischev.tm.model.ProjectDTO;

import java.util.ArrayList;
import java.util.Collection;

@Category(IntegrationCategory.class)
public class ProjectRestEndpointImplTest {

    @NotNull
    private ProjectRestEndpointClient client = ProjectRestEndpointClient.client();

    @NotNull
    private Collection<ProjectDTO> projects = new ArrayList<>();

    @NotNull
    private final ProjectDTO project1 = new ProjectDTO("Test Project 1");

    @NotNull
    private final ProjectDTO project2 = new ProjectDTO("Test Project 2");

    @NotNull
    private final ProjectDTO project3 = new ProjectDTO("Test Project 3");

    @Before
    public void initTest() {
        client.save(project1);
        client.save(project2);
        client.save(project3);
        projects = client.findAll();
    }

    @After
    public void clean() {
        client.delete(project1);
        client.delete(project2);
        client.delete(project3);
    }

    @Test
    public void testFindAll() {
        Collection<ProjectDTO> testFind = client.findAll();
        Assert.assertEquals(projects.size(), testFind.size());
    }

    @Test
    public void testFindById() {
        ProjectDTO project = client.findById(project1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getId(), project1.getId());
    }

    @Test
    public void testDelete() {
        @NotNull final String id = project2.getId();
        client.delete(project2);
        Assert.assertNull(client.findById(id));
    }

    @Test
    public void testDeleteById() {
        @NotNull final String id = project1.getId();
        client.deleteById(id);
        Assert.assertNull(client.findById(id));
    }

    @Test
    public void testSave() {
        @NotNull final ProjectDTO project4 = new ProjectDTO("Test Project 4");
        client.save(project4);
        Assert.assertNotNull(project4);
        String projectId = project4.getId();
        ProjectDTO projectTest = client.findById(projectId);
        Assert.assertNotNull(projectTest);
        client.delete(project4);
    }

}